import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
const path = require("path")
// import styleImport from 'vite-plugin-style-import';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(),
    // styleImport({
    //   libs: [{
    //     libraryName: 'element-plus',
    //     esModule: true,
    //     resolveStyle: (name) => {
    //       return `element-plus/theme-chalk/${name}.css`;
    //     },
    //     // resolveComponent: (name) => {
    //     //   let arr = name.split('-')
    //     //   return `element-plus/lib/conponents/${arr[1]}`;
    //     // },
    //   }]
    // })
  ],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src')
    }
  },
  publicDir: 'public',
  server: {
    // cors: true, // 默认启用并允许任何源
    host: 'localhost',
    port: 3000,
    open: true,// 在服务器启动时自动在浏览器中打开应用程序
    strictPort: false,
    https: false,

    // 反向代理
    //反向代理配置，注意rewrite写法，开始没看文档在这里踩了坑
    proxy: {
      '/robin': {
        target: 'http://192.168.25.43:38081/public/api/robin',
        // target: 'http://10.89.7.225:38081/public/api/robin',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/robin/, '')
      },
      '/api': {
        target: 'http://192.168.25.43:38081/api',
        // target: 'http://10.89.7.225:38081/public/api/robin',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, '')
      },
    }
  },
  build: {
    // target: 'modules',
    outDir: 'ar-map', //指定输出路径
    // assetsDir: 'assets', // 指定生成静态资源的存放路径
    // minify: 'terser' // 混淆器，terser构建后文件体积更小
  }
})
