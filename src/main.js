import { createApp } from 'vue'
import App from './App.vue'
import Router from './router/index.js'
import Vuex from './store/index.js'
import 'element-plus/dist/index.css'
import { 
    ElTree
} from 'element-plus';

const app = createApp(App)
app.use(ElTree)

app.use(Router).use(Vuex).mount('#app')