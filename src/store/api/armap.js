import api from "./armapUrl";
import { axios } from "@/utils/request";

// 根据标签查摄像头
export function queryCameraByLabelType(params) {
  return axios({
    url: api.queryCameraByLabelType,
    method:'post',
    data: params
  });
}
// 标签列表
export function labelType(params) {
  return axios({
    url: api.labelType,
    params: params
  });
}
// poi列表
export function QueryPoiResType(params) {
  return axios({
    url: api.QueryPoiResType,
    params: params
  });
}
// 获取初级节点
export function rootZone(params) {
  return axios({
    url: api.rootZone,
    params: params
  });
}
// 获取次级节点
export function subZones(params) {
  return axios({
    url: api.subZones,
    params: params
  });
}
// 获取相机列表
export function streamCameras(params) {
  return axios({
    url: api.streamCameras,
    params: params
  });
}
// 链接视频
export function connect(params) {
  return axios({
    url: api.connect,
    method:'post',
    data: params
  });
}
// 高点
export function hightCams(params) {
  return axios({
    url: api.hightCams,
    params: params
  });
}
// 开局所有撒点
export function allCams(params) {
  return axios({
    url: api.allCams,
    params: params
  });
}
// poi撒点
export function QueryPoiRes(params) {
  return axios({
    url: api.QueryPoiRes,
    data: params,
    method:'post'
  });
}
// 获取地图信息
export function getMapConfig(params) {
  return axios({
    params: params,
    url: api.getMapConfig,
  });
}
// 获取高点树1
export function getHPointTree1(params) {
  return axios({
    params: params,
    url: api.getHPointTree1,
  });
}
// 获取高点树2
export function getHPointTree2(params) {
  return axios({
    params: params,
    url: api.getHPointTree2,
  });
}