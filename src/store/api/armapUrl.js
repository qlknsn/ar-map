const api = {
    // 查找相机
    queryCameraByLabelType:'/robin/v1/map/queryCameraByLabelType',
    // 获取标签列表
    labelType:'/robin/v1/map/labelType',
    // 获取poi列表
    QueryPoiResType:'/api/v1/QueryPoiResType',
    // 获取poi撒点
    QueryPoiRes:'/api/v1/QueryPoiRes',
    // 获取初级节点
    rootZone:'/robin/v1/platform/bearhunting/rootZone',
    // 获取下层节点
    subZones:'/robin/v1/platform/bearhunting/subZones',
    // 获取摄像头
    streamCameras:'/robin/v1/platform/bearhunting/streamCameras',
    // 播放摄像头
    connect:'/robin/v1/platform/connect',
    // 高点摄像头
    hightCams:'/robin/v1/map/hightCams',
    // 开局所有撒点
    allCams:'/robin/v1/map/cams',
    // 获取地图信息
    getMapConfig: "/robin/v1/map/getConfig",
    // 获取高点树1
    getHPointTree1:"/robin/v1/platform/bearhunting/highStreamCameras",
    // 获得高点树2
    getHPointTree2:"/robin/v1/frontend/searchTree",
}
export default api;