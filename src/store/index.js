import { createStore } from 'vuex'
import armap from '@/store/modules/armap'

const store = createStore({
  state: {

  },
  getters: {

  },
  mutations: {

  },
  actions: {

  },
  modules: {
    armap
  }
})

export default store