import AMapLoader from "@amap/amap-jsapi-loader";
import { selectdeviceIcon } from "@/utils/deviceIcon";
import store from "@/store/index";
import { createApp, defineComponent, h, createVNode } from "vue";
import mapinfo from "@/components/mapInfo.vue";
import {
  queryCameraByLabelType,
  labelType,
  rootZone,
  subZones,
  streamCameras,
  connect,
  hightCams,
  allCams,
  QueryPoiResType,
  QueryPoiRes,
  getMapConfig,
  getHPointTree1,
  getHPointTree2,
} from "@/store/api/armap";
const state = {
  map: null,
  videomarkerList: [],
  cluster: null,
  singlecameraInfo: false,
  labelTypeList: [],
  selectedAllLabel: [],
  lingangLine: [
    [121.488536, 31.014952],
    [121.491926, 31.018594],
    [121.941502, 31.001996],
    [121.978516, 30.918439],
    [121.976685, 30.882515],
    [121.968992, 30.876097],
    [121.966868, 30.875351],

    [121.963419, 30.874085],
    [121.962319, 30.873749],
    [121.958709, 30.87245],
    [121.933265, 30.863066],
    [121.92067, 30.858259],
    [121.920836, 30.857927],
    [121.916915, 30.856306],

    [121.891836, 30.845696],
    [121.885142, 30.845376],
    [121.882223, 30.846776],
    [121.878758, 30.846168],
    [121.851442, 30.847964],
    [121.850783, 30.84527],
    [121.843846, 30.844947],
    [121.839897, 30.842866],
    [121.82106, 30.843164],
    [121.804966, 30.845302],
    [121.787757, 30.846167],
    [121.734499, 30.845025],
    [121.733735, 30.847328],
    [121.716204, 30.84731],
    [121.706666, 30.846121],
    [121.705411, 30.844721],
    [121.660312, 30.841483],
    [121.659502, 30.840189],
    [121.619398, 30.834422],
    [121.619516, 30.831097],
    [121.533481, 30.808882],
  ],
  rootZone: {},
  videoConnect: "",
  cameraInfo: false,
  sindevId: "",
  selectedSinglebing: [],
  singleCluster: null,
  isdanbingList: false,
  gaodiancluster: null,
  cMarkerList: [],
  sMarkerList: [],
  // 搜索出来的坐标
  searchmarkerArr: [],
  devname: "",
  gaodianmarkerArr: [],
  poiresList: [],
  allPoiSelect: [],
  poimarkerArr: [],
  highPointArr: [],
  HPointTree1: [],
  HPointTree2: [],
  polygonPathList: [],
  allCameras: [],
  mouseTool: ""
};
const actions = {
  // 获取poi列表
  QueryPoiResType({ commit }, params) {
    let p = QueryPoiResType(params);
    p.then((res) => {
      console.log(res)
      commit("GET_POIRES_LIST", res.data);
    });
  },
  // 获取poi撒点
  QueryPoiRes({ commit }, params) {
    let p = QueryPoiRes(params);
    p.then((res) => {
      console.log(res)
      commit("ADD_POIRES_MARKER", res.data);
    });
  },
  // 获得高点撒点
  QueryHPoint({ commit }, params) {
    // console.log(params);

    commit("ADD_HIGH_POINT", params)
  },
  // 获取所有摄像头
  allCams({ commit }, params) {
    let p = allCams(params);
    p.then((res) => {
      commit("ADD_ALL_MARKERS", res.data);
    });
  },
  // 获取播放流
  connect({ commit }, params) {
    let p = connect(params);
    p.then((res) => {
      commit("SHOW_CAMERA_WINDOW", true);
      commit("CONNECT_VIDEO", res);
    });
  },
  // 获取节点下相机列表
  streamCameras({ commit }, params) {
    let p = streamCameras(params);
    commit;
    return p;
  },
  // 获取次级节点
  subZones({ commit }, params) {
    let p = subZones(params);
    commit;
    return p;
  },
  // 获取初级节点
  rootZone({ commit }, params) {
    let p = rootZone(params);
    p.then((res) => {
      commit("GET_ROOT_ZONE", res.result.result);
    });
  },
  // 调用视频撒点接口
  queryCameraByLabelType({ commit }, params) {
    let p = queryCameraByLabelType(params);
    p.then((res) => {
      // 撒点
      commit("ADD_MARKER", res.data);
    });
  },
  // 调用单兵视频撒点接口
  querySingleCameraByLabelType({ commit }, params) {
    let p = queryCameraByLabelType(params);
    p.then((res) => {
      //
      // 撒点
      commit("ADD_DANBING_MARKER", { data: res.data, type: "dian" });
    });
  },
  // 调用相机标签列表接口
  labelType({ commit }, params) {
    let p = labelType(params);
    p.then((res) => {
      // console.log(res)
      commit("GET_LABELTYPE_LIST", res);
    });
  },
  // 获取高点
  hightCams({ commit }, params) {
    let p = hightCams(params);
    p.then((res) => {
      res.data.forEach((item) => {
        item.markType = "高点";
      });
      // console.log(res)
      commit("ADD_GAODIAN_LIST", res.data);
    });
  },
  // 获取地图信息
  getMapConfig({ commit }, data) {
    let p = getMapConfig(data);
    return p;
  },
  // 获取高点树1
  getHPointTree1({ commit }, data) {
    let p = getHPointTree1(data);
    p.then(res => {
      commit("GET_HIGH_POINT_TREE_1", res);
    })
  },
  // 获取高点树2
  getHPointTree2({ commit }, data) {
    let p = getHPointTree2(data);
    p.then(res => {
      commit("GET_HIGH_POINT_TREE_2", res);
    })
  },
};
const mutations = {
  ADD_POIRES_MARKER(state, res) {
    store.commit('CLEAR_POI_MARKER')
    res.forEach((item) => {
      console.log(item)
      var startIcon = new AMap.Icon({
        // 图标尺寸
        size: new AMap.Size(18, 22),
        // 图标的取图地址
        image: selectdeviceIcon(item.type),
        // 图标所用图片大小
        imageSize: new AMap.Size(18, 22),
      });

      const marker = new AMap.Marker({
        icon: startIcon,
        position: [item.longitude, item.latitude], //循环经纬度
        title: item.name, //标题
      });
      state.poimarkerArr.push(marker);
      marker.setLabel({
        offset: new AMap.Pixel(0, 0),
        // content: `<div style='width:150px;height:50px;word-wrap: break-word;word-break: normal;display:flex;align-items:center;'>${item.name}</div>`,
      });
    });
    if (state.map.getZoom() < 14.5) {
      // state.map.remove(state.poimarkerArr)
    } else {
      state.map.add(state.poimarkerArr)
    }
    // state.map.add(state.poimarkerArr);
  },
  ADD_HIGH_POINT(state, res) {
    var { item, length } = res;
    res = item;
    // console.log(res);
    // console.log(length);
    // 判断是否有选中
    if (length == 0) {
      // 没有选中就展示全部
      store.commit('CLEAR_HIGH_POINT');
      store.dispatch("hightCams");
    } else {
      // 先清除上次选中的内容
      store.commit('CLEAR_HIGH_POINT')
      // 遍历树
      var { children } = res;
      var paramsList = [];
      // 选中高点的定位
      if (children) {
        for (let child of children) {
          if (child.children) {
            for (let node of child.children) {

              if (node.children) {
                for (let leaf of node.children) {
                  // console.log(leaf);
                  paramsList.push(leaf);
                }
              } else {
                // console.log(node);
                paramsList.push(node);
              }
            }
          } else {
            // console.log(child);
            paramsList.push(child);
          }
        }
      } else {
        paramsList.push(res);
      }
      // console.log(paramsList);
      store.commit("SET_MARKER", paramsList);
    }
  },
  // 高点撒点
  SET_MARKER(state, res) {
    // 方法一：循环传入单个标记，无法进行聚合点设置
    // console.log(res);
    // if (res.type == "hightCamera") {
    //   let startIcon = new AMap.Icon({
    //     // 图标尺寸
    //     size: new AMap.Size(36, 55),
    //     // 图标的取图地址
    //     image: selectdeviceIcon("高点"),
    //     // 图标所用图片大小
    //     imageSize: new AMap.Size(36, 55),
    //   });

    //   const marker = new AMap.Marker({
    //     icon: startIcon,
    //     position: [res.longitude, res.latitude], //循环经纬度
    //     title: "高点", //标题
    //   });

    //   state.highPointArr.push(marker);
    //   marker.setLabel({
    //     offset: new AMap.Pixel(0, 0),
    //     // content: `<div style='width:150px;height:50px;word-wrap: break-word;word-break: normal;display:flex;align-items:center;'>${item.name}</div>`,
    //   });
    //   marker.on("click", e => {
    //     console.log(e);
    //     // console.log(res);
    //     Object.assign(res, { arCameraType: "ar" });
    //     // console.log(res);
    //     store.commit("TO_AR", res);
    //   })
    // } else if (res.type == "camera") {
    //   let startIcon = new AMap.Icon({
    //     // 图标尺寸
    //     size: new AMap.Size(36, 55),
    //     // 图标的取图地址
    //     image: selectdeviceIcon("高点"),
    //     // 图标所用图片大小
    //     imageSize: new AMap.Size(36, 55),
    //   });

    //   const marker = new AMap.Marker({
    //     icon: startIcon,
    //     position: [res.lng, res.lat], //循环经纬度
    //     title: "高点", //标题
    //   });

    //   console.log(marker);

    //   state.highPointArr.push(marker);
    //   marker.setLabel({
    //     offset: new AMap.Pixel(0, 0),
    //     // content: `<div style='width:150px;height:50px;word-wrap: break-word;word-break: normal;display:flex;align-items:center;'>${item.name}</div>`,
    //   });
    // }

    // state.map.add(state.highPointArr);

    // 方案二 将选中的点以列表的形式传入，可以通过聚合点的方式撒点
    console.log(res);

    let clusterPoints = [];
    res.forEach((item) => {
      // console.log(item);
      if (item.longitude && item.latitude) {
        clusterPoints.push({
          lnglat: [item.longitude, item.latitude],
          itemdata: item,
        });
      }
      // 聚合点需传的参数
    });
    // 自定义撒点样式
    var _renderMarker = function (context) {
      // console.log(context)
      var content = `<div style="">
                <img style='width:35px;height:60px' src="${selectdeviceIcon(
        "高点"
      )}"/>
            </div>`;
      var offset = new AMap.Pixel(-9, -50);
      context.marker.setContent(content);
      context.marker.setOffset(offset);
    };
    // console.log(state.clusterPoints)
    // 高德官网提供的示例不是最新的AMap.MarkerCluster ,而不是AMap.MarkerClusterer
    state.map.plugin(["AMap.MarkerCluster"], function () {
      // console.log(clusterPoints)
      state.gaodiancluster = new AMap.MarkerClusterer(
        state.map,
        clusterPoints,
        {
          renderMarker: _renderMarker,
          gridSize: 60,
          clusterByZoomChange: false,
        }
      );

      // 点击聚合点的回调
      // console.log(state.gaodiancluster);
      state.gaodiancluster.on("click", function (e) {
        console.log(e.clusterData);
        if (e.clusterData.length > 1) {
          // 多于一个点的聚合点点击事件，弹出聚合点框
          store.commit("SHOW_TOGETHER_WINDOW", e.clusterData);
        } else {
          // 单独一个点的点击事件弹出视频框
          console.log(e.clusterData[0].itemdata)
          Object.assign(e.clusterData[0].itemdata, { arCameraType: "ar" });
          store.commit("TO_AR", e.clusterData[0].itemdata);
        }
      });
    });
  },
  GET_POIRES_LIST(state, res) {
    state.allPoiSelect = res
    let arr = []
    res.forEach((item, index) => {
      arr.push({ selected: false, typeName: item, id: index })
    })
    state.poiresList = arr
  },
  GET_LUKUANG(state, res) {
    state.map.addControl(
      new AMap.MapType({
        defaultType: 0, //0代表默认，1代表卫星
      })
    );
  },
  CAMERA_SEARCH(state, res) {
    console.log(state.cMarkerList.length);
    console.log(state.sMarkerList);
    console.log(res);
    if (state.cMarkerList.length == 0 && state.sMarkerList.length == 0) {
      alert("地图上暂时没有点标记");
      return false;
    }
    let c = [];
    let s = [];
    // let g = []
    state.cMarkerList.forEach((item) => {
      let a = item.devName.indexOf(res);
      if (a !== -1) {
        c.push(item);
      }
    });
    state.sMarkerList.forEach((item) => {
      let a = item.devName.indexOf(res);
      if (a !== -1) {
        s.push(item);
      }
    });
    let arr = [...c, ...s];
    store.commit("ADD_SEARCH_MARKER", arr);
  },
  CLEAR_POI_MARKER(state) {
    state.map.remove(state.poimarkerArr);
    state.poimarkerArr = []
  },
  CLEAR_HIGH_POINT(state) {
    state.gaodiancluster.setMap(null);
    state.highPointArr = []
  },
  CLEAR_SEARCH_MARKER(state) {
    state.map.remove(state.searchmarkerArr);
    state.searchmarkerArr = []

  },
  ADD_SEARCH_MARKER(state, res) {
    // state.searchmarkerArr
    res.forEach((item) => {
      const marker = new AMap.Marker({
        icon: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/map_icon/touming.png",
        position: [item.lng, item.lat], //循环经纬度
        title: item.devName, //标题
      });
      state.searchmarkerArr.push(marker);
      marker.setLabel({
        content: item.devName,
      });
    });
    state.map.add(state.searchmarkerArr);
  },
  CONNECT_VIDEO(state, res) {
    console.log(res);
    // 视频流地址
    state.videoConnect = res.result.metadata.url;
  },
  GET_ROOT_ZONE(state, res) {
    // console.log(res)
    // 赋值初级节点
    res.urns = `urn:${res.urn.organization}:${res.urn.group}:${res.urn.id}`;
    state.rootZone = res;
  },
  GET_LABELTYPE_LIST(state, res) {
    res.data.forEach((res) => {
      // 默认给所有标签不选中状态
      res.selected = false;
      // 获取全选时候标签参数，并单独划出单兵设备
      if (res.typeName == "单兵") {
        state.selectedSinglebing.push({ typeName: res.typeName });
      } else {
        state.selectedAllLabel.push({ typeName: res.typeName });
      }
    });
    state.labelTypeList = res.data;
  },
  INIT_MAP(state, res) {
    console.log(res);
    
    AMapLoader.load({
      key: "cb8153e93ef8ca4c36d0b325bd4e069c", // 申请好的Web端开发者Key，首次调用 load 时必填
      version: "2.0", // 指定要加载的 JSAPI 的版本，缺省时默认为 1.4.15
      plugins: ["AMap.MarkerClusterer", "AMap.MapType", "AMap.MouseTool"], // 需要使用的的插件列表，如比例尺'AMap.Scale'等
      AMapUI: {
        // 是否加载 AMapUI，缺省不加载
        version: "1.1", // AMapUI 缺省 1.1
        plugins: [], // 需要加载的 AMapUI ui插件
      },
      Loca: {
        // 是否加载 Loca， 缺省不加载
        version: "2.0", // Loca 版本，缺省 1.3.2
      },
      mapStyle: "amap://styles/a5352be2fab74f4856fbe9781e70a59b",
    })
      .then((AMap) => {
        state.map = new AMap.Map("container", {
          zoom: 10, //初始地图级别
          viewMode: "3D",
          center: [121.902659, 30.980113], //初始地图中心点
          showIndoorMap: true, //显示地图自带的室内地图图层
          resizeEnable: true,
        });
        // var circle = new AMap.Circle({
        //   center: [res.longitude, res.latitude],
        //   radius: 3000,
        //   fillColor: '#C1DFFA',   // 圆形填充颜色
        //   strokeColor: '#7280FA', // 描边颜色
        //   strokeWeight: 1, // 描边宽度
        //   strokeStyle: "dashed", //轮廓线样式：虚线
        // });
        // if(res.range){
        //   circle.setRadius(1000*res.range);
        // }
        // circle.setMap(state.map);   
        state.map.setMapStyle('amap://styles/a5352be2fab74f4856fbe9781e70a59b')
        store.commit("POLYGON");
        store.dispatch("hightCams");
        store.commit("GET_LUKUANG");
        store.dispatch("allCams");
        state.map.setZoomAndCenter(res.zoom, [res.longitude, res.latitude]);
        state.map.on("zoomchange", () => {
          console.log(state.map.getZoom())
          if (state.map.getZoom() < 14.5) {
            state.map.remove(state.poimarkerArr)
          } else {
            state.map.add(state.poimarkerArr)
          }
        })
      })
      .catch((e) => {
        console.log(e);
      });
  },
  DRAW(state) {

    console.log(state.cMarkerList);
    state.mouseTool = new AMap.MouseTool(state.map);
    //用鼠标工具画多边形
    var drawPolygon = state.mouseTool.polygon({
      strokeColor: '#80d8ff',
      strokeOpacity: 0.2,
      fillOpacity: 0.2,
    });
    state.mouseTool.on("draw", (event) => {
      // console.log(event.obj.getPath());
      let path = event.obj.getPath();
      state.polygonPathList = path.map(item => {
        return [item.lng, item.lat];
      });
      // console.log(state.polygonPathList);
      let po = [];
      let pointInRegion = [];
      // 如果搜索的标记列表为空（没有进行筛选的操作进入如下循环）
      if (state.cMarkerList.length == 0) {
        // 遍历所有相机，将框选范围内的点和所有点进行比对
        // item:所有相机
        state.allCameras.forEach(item => {
          item = item.itemdata;
          // console.log(item.itemdata);
          if (item.lat && item.lng) {
            po = [item.lng, item.lat];
          }
          if (po.length != 0) {
            // console.log(po);
            // 判断相机是否在框选范围内
            let isInSet = AMap.GeometryUtil.isPointInRing(po, state.polygonPathList);
            // console.log(isInSet);
            // 如果在框选范围内则将这个点加入pointInRegion数组
            if (isInSet) {
              pointInRegion.push(po);
            }
          }
        })
        // 如果进行了筛选操作执行如下循环
      } else {
        // 在筛选的相机列表中遍历，并和框选范围进行比对
        state.cMarkerList.forEach(item => {
          if (item.lat && item.lng) {
            po = [item.lng, item.lat];
          }
          if (po.length != 0) {
            // console.log(po);
            // 判断相机是否在框选范围内
            let isInSet = AMap.GeometryUtil.isPointInRing(po, state.polygonPathList);
            // console.log(isInSet);
            // 如果在框选范围内则将这个点加入pointInRegion数组
            if (isInSet) {
              pointInRegion.push(po);
            }
          }
        })
      }

      let params = [];
      for (let val of pointInRegion) {
        if (state.cMarkerList.length == 0) {
          for (let temp of state.allCameras) {
            temp = temp.itemdata;
            if (temp.lng == val[0] && temp.lat == val[1]) {
              params.push(temp);
            }
          }
        } else {
          for (let temp of state.cMarkerList) {
            if (temp.lng == val[0] && temp.lat == val[1]) {
              params.push(temp);
            }
          }
        }
      }
      console.log(params);
      // mouseTool = null;

      store.commit("TO_AR2", params);
    })
  },
  TO_AR2(state, f) {
    // console.log(window.BgisInvokeWpf);
    // console.log(window.BgisInvokeWpf.jsInvokeWPF);
    var devices = [];
    for (let val of f) {
      devices.push({
        id: val.devId ? val.devId : val.id,
        type: "camera",
        cameraName: val.devName ? val.devName : val.name
      })
    }
    console.log(devices);
    if (
      typeof window.BgisInvokeWpf != "undefined" &&
      typeof window.BgisInvokeWpf.jsInvokeWPF == "function"
    ) {
      let json = {
        eventType: "regionPoint",
        devices: devices
      };
      console.log(json);
      console.log("调用函数：window.BgisInvokeWpf.jsInvokeWPF");

      window.BgisInvokeWpf.jsInvokeWPF(JSON.stringify(json));
      state.mouseTool.close(true);
    } else {
      console.log("no BgisInvokeWpf");
      state.mouseTool.close(true);
    }
  },
  POLYGON(state) {
    var polygon = new AMap.Polygon({
      path: state.lingangLine,
      strokeColor: "#40c4e8",
      strokeWeight: 4,
      strokeStyle: "dashed",
      fillColor: "transparent",
      zIndex: 50,
    });
    state.map.add(polygon);
    state.map.setFitView([polygon]);
  },
  CLEAR_CLUSTER(state) {
    // 清除撒点和聚合点

    if (state.cluster) {
      // state.map.clearMap()
      state.cluster.setMap(null);
      state.cluster = null;
    }
  },
  CLEAR_SINGLE_CLUSTER(state) {
    // 清除单兵撒点和聚合点

    if (state.singleCluster) {
      // state.map.clearMap()
      state.singleCluster.setMap(null);
      state.singleCluster = null;
    }
  },
  ADD_GAODIAN_LIST(state, res) {
    // res.forEach(item => {
    //     const marker = new AMap.Marker({
    //         icon:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/map_icon/biaoqian-qing_39_69.png',
    //         position: [item.longitude, item.latitude], //循环经纬度
    //         title: item.customName //标题
    //     })
    //     state.gaodianmarkerArr.push(marker)
    //     marker.setLabel({
    //         content: item.customName
    //     })
    // })
    // state.map.add(state.gaodianmarkerArr)

    // 获取高点集合的撒点
    state.gMarkerList = res;

    console.log(res);

    let clusterPoints = [];
    res.forEach((item) => {
      if (item.longitude && item.latitude) {
        clusterPoints.push({
          lnglat: [item.longitude, item.latitude],
          itemdata: item,
        });
      }
      // 聚合点需传的参数
    });
    // 自定义撒点样式
    var _renderMarker = function (context) {
      // console.log(context)
      var content = `<div style="">
                <img style='width:35px;height:60px' src="${selectdeviceIcon(
        context.data[0].itemdata.markType
      )}"/>
            </div>`;
      var offset = new AMap.Pixel(-9, -50);
      context.marker.setContent(content);
      context.marker.setOffset(offset);
    };
    // console.log(state.clusterPoints)
    // 高德官网提供的示例不是最新的AMap.MarkerCluster ,而不是AMap.MarkerClusterer
    state.map.plugin(["AMap.MarkerCluster"], function () {
      // console.log(clusterPoints)
      state.gaodiancluster = new AMap.MarkerClusterer(
        state.map,
        clusterPoints,
        {
          renderMarker: _renderMarker,
          gridSize: 60,
          clusterByZoomChange: false,
        }
      );

      // 点击聚合点的回调
      console.log(state.gaodiancluster);
      state.gaodiancluster.on("click", function (e) {
        console.log(e.clusterData[0]);
        if (e.clusterData.length > 1) {
          // 多于一个点的聚合点点击事件，弹出聚合点框
          store.commit("SHOW_TOGETHER_WINDOW", e.clusterData);
        } else {
          // 单独一个点的点击事件弹出视频框
          console.log(e.clusterData[0].itemdata)
          Object.assign(e.clusterData[0].itemdata, { arCameraType: "ar" });
          store.commit("TO_AR", e.clusterData[0].itemdata);
        }
      });
    });
  },
  TO_AR(state, f) {
    // console.log(window.BgisInvokeWpf);
    // console.log(window.BgisInvokeWpf.jsInvokeWPF);
    console.log({
      eventType: "markerClick",
      devices: [
        {
          id: f.id,
          type: f.arCameraType == "ar" ? "arcamera" : "camera",
          cameraName: f.name ? f.name : f.customName
        },
      ],
    });
    if (
      typeof window.BgisInvokeWpf != "undefined" &&
      typeof window.BgisInvokeWpf.jsInvokeWPF == "function"
    ) {
      let json = {
        eventType: "markerClick",
        devices: [
          {
            id: f.id,
            type: f.arCameraType == "ar" ? "arcamera" : "camera",
            cameraName: f.name ? f.name : f.customName
          },
        ],
      };
      console.log("调用函数：window.BgisInvokeWpf.jsInvokeWPF");

      window.BgisInvokeWpf.jsInvokeWPF(JSON.stringify(json));
    } else {
      console.log("no BgisInvokeWpf");
    }
  },
  formatOutputInfo(state, device) {
    let rs = {
      id: device.id,
      // devUrl: device.devUrl,
      type: "arcamera",
      // markId: device.markId,
      // cameraName: device.cameraName
    };
    return rs;
  },
  ADD_ALL_MARKERS(state, res) {
    // 存储普通视频点的撒点集合
    store.commit("CLEAR_CLUSTER");
    let clusterPoints = [];
    res.forEach((item) => {
      if (item.lng && item.lat && item.type == "camera") {
        // 聚合点需传的参数
        clusterPoints.push({
          lnglat: [item.lng, item.lat],
          itemdata: item,
        });
      }
    });
    state.allCameras = clusterPoints;
    // 自定义撒点样式
    var _renderMarker = function (context) {
      console.log(context);
      var content = `<div style="">
                 <img style='width:30px;height:45px' src="${selectdeviceIcon(
        context.data[0].itemdata.type
      )}"/>
             </div>`;
      var offset = new AMap.Pixel(-9, -9);
      context.marker.setContent(content);
      context.marker.setOffset(offset);
    };
    // console.log(state.clusterPoints)
    // 高德官网提供的示例不是最新的AMap.MarkerCluster ,而不是AMap.MarkerClusterer
    state.map.plugin(["AMap.MarkerCluster"], function () {
      // console.log(clusterPoints)
      state.cluster = new AMap.MarkerClusterer(state.map, clusterPoints, {
        renderMarker: _renderMarker,
        gridSize: 60,
        clusterByZoomChange: false,
      });

      // 点击聚合点的回调
      state.cluster.on("click", function (e) {
        // console.log(e)
        if (e.clusterData.length > 1) {
          // 多于一个点的聚合点点击事件，弹出聚合点框
          store.commit("SHOW_TOGETHER_WINDOW", e.clusterData);
        } else {

          Object.assign(e.cluster.m[0].itemdata, { arCameraType: "normal" });
          store.commit("TO_AR", e.cluster.m[0].itemdata);
          // console.log(e.clusterData[0]);

          // console.log(e);
          // let params = {
          //   protocol: "HLS",
          //   level: "PRIMARY",
          //   cameraUrn: e.clusterData[0].itemdata.id,
          //   adapterType: "hikvision",
          //   force: true,
          // };
          // store.commit("GET_DEV_NAME", e.clusterData[0].itemdata.name);
          // store.dispatch("connect", params);
          // e.clusterData[0].itemdata.devId
          // store.commit("SHOW_CAMERA_WINDOW", true);
        }
      });
    });
  },
  ADD_MARKER(state, res) {
    // 存储普通视频点的撒点集合
    state.cMarkerList = res;
    store.commit("CLEAR_CLUSTER");
    let clusterPoints = [];
    res.forEach((item) => {
      if (item.lng && item.lat) {
        // 聚合点需传的参数
        clusterPoints.push({
          lnglat: [item.lng, item.lat],
          itemdata: item,
        });
      }
    });
    // 自定义撒点样式
    var _renderMarker = function (context) {
      // console.log(context)
      var content = `<div style="">
                <img style='width:30px;height:45px' src="${selectdeviceIcon(
        context.data[0].itemdata.markType
      )}"/>
            </div>`;
      var offset = new AMap.Pixel(-9, -9);
      context.marker.setContent(content);
      context.marker.setOffset(offset);
    };
    // console.log(state.clusterPoints)
    // 高德官网提供的示例不是最新的AMap.MarkerCluster ,而不是AMap.MarkerClusterer
    state.map.plugin(["AMap.MarkerCluster"], function () {
      // console.log(clusterPoints)
      state.cluster = new AMap.MarkerClusterer(state.map, clusterPoints, {
        renderMarker: _renderMarker,
        gridSize: 60,
        clusterByZoomChange: false,
      });

      // 点击聚合点的回调
      state.cluster.on("click", function (e) {
        console.log(e)
        if (e.clusterData.length > 1) {
          // 多于一个点的聚合点点击事件，弹出聚合点框
          store.commit("SHOW_TOGETHER_WINDOW", e.clusterData);
        } else {
          // 单独一个点的点击事件弹出视频框
          if (e.cluster.m[0].itemdata.markType == "单兵") {
            // 获取终端Id
            state.sindevId = e.cluster.m[0].itemdata.devId.split("db:")[1];
            store.commit("SHOW_SINGLE_WINDOW", true);
          } else {
            Object.assign(e.cluster.m[0].itemdata, { arCameraType: "normal" });
            store.commit("TO_AR", e.cluster.m[0].itemdata);
            // console.log(e);
            // let params = {
            //   protocol: "HLS",
            //   level: "PRIMARY",
            //   cameraUrn: e.clusterData[0].itemdata.devId,
            //   adapterType: "hikvision",
            //   force: true,
            // };
            // store.commit("GET_DEV_NAME", e.clusterData[0].itemdata.devName);
            // store.dispatch("connect", params);
            // e.clusterData[0].itemdata.devId
            // store.commit("SHOW_CAMERA_WINDOW", true);
          }
        }
      });
    });
  },
  GET_DEV_NAME(state, res) {
    state.devname = res;
  },
  // 设置状态是够勾选单兵
  SHOW_DANBING_LIST(state, res) {
    state.isdanbingList = res;
  },
  ADD_DANBING_MARKER(state, { data, type }) {
    // 获取单兵设备的撒点结合
    state.sMarkerList = data;
    // 点击标签的撒点直接撒
    if (type == "dian") {
      store.commit("CLEAR_SINGLE_CLUSTER");
      console.log(state.singleCluster);
      let singleclusterPoints = [];
      data.forEach((item) => {
        if (item.lng && item.lat) {
          // 聚合点需传的参数
          singleclusterPoints.push({
            lnglat: [item.lng, item.lat],
            itemdata: item,
          });
        }
      });
      // 自定义撒点样式
      var _renderMarker = function (context) {
        // console.log(context)
        var content = `<div style="">
                <img style='width:30px;height:40px' src="${selectdeviceIcon(
          `${context.data[0].itemdata.markType}_${context.data[0].itemdata.status}`
        )}"/>
            </div>`;
        var offset = new AMap.Pixel(-9, -9);
        context.marker.setContent(content);
        context.marker.setOffset(offset);
      };
      // console.log(state.clusterPoints)
      // 高德官网提供的示例不是最新的AMap.MarkerCluster ,而不是AMap.MarkerClusterer
      state.map.plugin(["AMap.MarkerCluster"], function () {
        state.singleCluster = new AMap.MarkerClusterer(
          state.map,
          singleclusterPoints,
          {
            renderMarker: _renderMarker,
            gridSize: 60,
            clusterByZoomChange: false,
          }
        );
        // 点击聚合点的回调
        state.singleCluster.on("click", function (e) {
          // console.log(e)
          if (e.clusterData.length > 1) {
            // 多于一个点的聚合点点击事件，弹出聚合点框
            store.commit("SHOW_TOGETHER_WINDOW", e.clusterData);
          } else {
            // 单独一个点的点击事件弹出视频框
            if (e.clusterData[0].itemdata.markType == "单兵") {
              // 获取终端Id
              state.sindevId = e.clusterData[0].itemdata.devId.split("db:")[1];
              store.commit("GET_DEV_NAME", e.clusterData[0].itemdata.devName);
              store.commit("SHOW_SINGLE_WINDOW", true);
            } else {
              // console.log(e);
              let params = {
                protocol: "HLS",
                level: "PRIMARY",
                cameraUrn: e.clusterData[0].itemdata.devId,
                adapterType: "hikvision",
                force: true,
              };
              store.dispatch("connect", params);
              // e.clusterData[0].itemdata.devId
              // store.commit("SHOW_CAMERA_WINDOW", true);
            }
          }
        });
      });
    } else {
      console.log(state.isdanbingList);
      // 推送过来的数据,判断是否勾选单兵
      if (state.isdanbingList) {
        //勾选单兵，则跟新数据
        store.commit("CLEAR_SINGLE_CLUSTER");
        // console.log(state.singleCluster);
        let singleclusterPoints = [];
        data.forEach((item) => {
          if (item.lng && item.lat) {
            // 聚合点需传的参数
            singleclusterPoints.push({
              lnglat: [item.lng, item.lat],
              itemdata: item,
            });
          }
        });
        // 自定义撒点样式
        var _renderMarker = function (context) {
          // console.log(context)
          var content = `<div style="">
                <img style='width:30px;height:45px' src="${selectdeviceIcon(
            `${context.data[0].itemdata.markType}_${context.data[0].itemdata.status}`
          )}"/>
            </div>`;
          var offset = new AMap.Pixel(-9, -9);
          context.marker.setContent(content);
          context.marker.setOffset(offset);
        };
        // console.log(state.clusterPoints)
        // 高德官网提供的示例不是最新的AMap.MarkerCluster ,而不是AMap.MarkerClusterer
        state.map.plugin(["AMap.MarkerCluster"], function () {
          state.singleCluster = new AMap.MarkerClusterer(
            state.map,
            singleclusterPoints,
            {
              renderMarker: _renderMarker,
              gridSize: 60,
              clusterByZoomChange: false,
            }
          );

          // 点击聚合点的回调
          state.singleCluster.on("click", function (e) {
            // console.log(e)
            if (e.clusterData.length > 1) {
              // 多于一个点的聚合点点击事件，弹出聚合点框
              store.commit("SHOW_TOGETHER_WINDOW", e.clusterData);
            } else {
              // 单独一个点的点击事件弹出视频框
              if (e.clusterData[0].itemdata.markType == "单兵") {
                // 获取终端Id
                state.sindevId =
                  e.clusterData[0].itemdata.devId.split("db:")[1];
                store.commit("GET_DEV_NAME", e.clusterData[0].itemdata.devName);
                store.commit("SHOW_SINGLE_WINDOW", true);
              } else {
                console.log(e);
                let params = {
                  protocol: "HLS",
                  level: "PRIMARY",
                  cameraUrn: e.clusterData[0].itemdata.devId,
                  adapterType: "hikvision",
                  force: true,
                };
                store.dispatch("connect", params);
                // e.clusterData[0].itemdata.devId
                // store.commit("SHOW_CAMERA_WINDOW", true);
              }
            }
          });
        });
      } else {
        // 没勾选单兵不处理
      }
    }
  },
  GET_SINGLE_DEVID(state, res) {
    state.sindevId = res;
  },
  SHOW_TOGETHER_WINDOW(state, res) {
    console.log(res);
    // 加载infowindow
    var infoWindow = new AMap.InfoWindow({
      content: `<div id='info'></div>`,
      // position: res[0].name,
      anchor: "bottom-center",
      offset: new AMap.Pixel(0, -20),
    });
    // 判断是什么类型 的摄像机
    if (res[0].itemdata.markType && res[0].itemdata.markType == "高点") {
      infoWindow.open(state.map, [
        res[0].itemdata.longitude,
        res[0].itemdata.latitude,
      ]);
    } else if (res[0].itemdata.type && res[0].itemdata.type == "hightCamera") {
      infoWindow.open(state.map, [
        res[0].itemdata.longitude,
        res[0].itemdata.latitude,
      ]);
    } else if(res[0].itemdata.type && res[0].itemdata.type == "camera"){
      infoWindow.open(state.map, [
        res[0].itemdata.lng,
        res[0].itemdata.lat,
      ]);
    } else {
      infoWindow.open(state.map, [res[0].itemdata.lng, res[0].itemdata.lat]);
    }

    // console.log(res)
    // 自定义聚合点框的样式
    const app = defineComponent({
      render() {
        return h("div", { class: "camBox" }, [
          h(mapinfo, { msg: res }), //引入聚合点弹框组件
        ]);
      },
    });
    createApp(app).mount("#info");
    // 设置弹出框位置
    infoWindow.setAnchor("bottom-center");
  },
  SHOW_SINGLE_WINDOW(state, res) {
    // console.log(res)
    // 更改单兵视频框的显示属性
    state.singlecameraInfo = res;
  },
  SHOW_CAMERA_WINDOW(state, res) {
    // console.log(res)
    // 更改视频框的显示属性
    state.cameraInfo = res;
  },
  GET_HIGH_POINT_TREE_1(state, res) {
    console.log(res.results);
    state.HPointTree1 = res.results;
  },
  GET_HIGH_POINT_TREE_2(state, res) {
    console.log(res.data);
    state.HPointTree2 = res.data;
  }
};
export default {
  state,
  actions,
  mutations,
};
