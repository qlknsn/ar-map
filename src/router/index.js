import { createRouter, createWebHashHistory } from 'vue-router'
import armap from '../views/armap.vue'

const Router = createRouter({
  history: createWebHashHistory(),
  routes: [{
    path: '/',
    name: 'armap',
    component: armap,
  }]
})

export default Router