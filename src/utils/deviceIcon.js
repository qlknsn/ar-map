const deviceIcons = {
    "监控点":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/%E4%B8%B4%E6%B8%AFAR%E5%9C%B0%E5%9B%BE%E5%88%87%E5%9B%BE%E7%B4%A0%E6%9D%90/shexiangji.png',
    "camera":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/%E4%B8%B4%E6%B8%AFAR%E5%9C%B0%E5%9B%BE%E5%88%87%E5%9B%BE%E7%B4%A0%E6%9D%90/shexiangji.png',
    "人卡":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/map_icon/ren.png',
    "车卡":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/map_icon/che.png',
    "WIFI":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/map_icon/wifi.png',
    "景点":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/map_icon/jingqu.png',
    "RFID":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/map_icon/RFID.png',
    "警力":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/map_icon/jingli.png',
    "执勤点":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/map_icon/zhiqindian.png',
    "小区":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/map_icon/xiaoqu.png',
    "楼宇":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/map_icon/louyu.png',
    "学校":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/map_icon/xuexiao.png',
    "道路":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/map_icon/daolu.png',
    "警察":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/map_icon/jingcha.png',
    "电子围栏":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/map_icon/dianziweilkan.png',
    "GPS标签":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/map_icon/GPS.png',
    "高点标签":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/map_icon/gaodian.png',
    "预案标签":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/map_icon/yuan.png',
    "单兵_1":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/%E4%B8%B4%E6%B8%AFAR%E5%9C%B0%E5%9B%BE%E5%88%87%E5%9B%BE%E7%B4%A0%E6%9D%90/danbing.png',
    "单兵_0":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/%E4%B8%B4%E6%B8%AFAR%E5%9C%B0%E5%9B%BE%E5%88%87%E5%9B%BE%E7%B4%A0%E6%9D%90/danbing_grey.png',
    "高点":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-manage/map_icon/biaoqian-qing_39_69.png',
    "住宿服务":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-map/poi/ic_map_accommodation_service.png',
    "商务住宅":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-map/poi/ic_map_building.png',
    "政府机构及社会团体":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-map/poi/ic_map_government.png',
    "体育休闲服务":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-map/poi/ic_map_leisure_sports.png',
    "生活服务":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-map/poi/ic_map_life.png',
    "医疗保健服务":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-map/poi/ic_map_medical_insurance.png',
    "通行设施":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-map/poi/ic_map_pass.png',
    "风景名胜":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-map/poi/ic_map_scenic_spot.png',
    "科教文化服务":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-map/poi/ic_map_science_education_and_culture.png',
    "购物服务":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-map/poi/ic_map_shopping.png',
    "交通设施服务":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-map/poi/ic_map_transportation_facilities.png',
    "公司企业":'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/ar-map/poi/ic_map_unit.png',
};

export function selectdeviceIcon(statu) {
    console.log(statu)
    return deviceIcons[statu];
}