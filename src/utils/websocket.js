import store from '@/store/index'

let socket = null
// const WS_BASE_URL = 'ws://192.168.25.43:38082/wsrobin'
// const WS_BASE_URL = 'ws://10.242.212.152:61616/wsrobin'
const WS_BASE_URL = 'ws://10.89.7.225:38082/wsrobin'
// const WS_BASE_URL = 'ws://ar-map.bearhunting.cn/wsrobin'
// const WS_BASE_URL = 'ws://192.168.63.20:38082/robin'

export function WSconnect() {
    // // 创建websocket 对象
    socket = new WebSocket(WS_BASE_URL)
    socket.onopen = () => {
        
        let jsons = {
            type: 'register',
            districtId: '123',
        }
        socket.send(JSON.stringify(jsons))
    }
    socket.onerror = () => {
        // reconnect()
    }
    socket.onmessage = (evt) => {
        let wsdata = JSON.parse(evt.data)
        console.log(wsdata)
        if(wsdata.data){
            store.commit('ADD_DANBING_MARKER',{ data:wsdata.data,type:'tui'})
        }
        // Vue;
    };
    socket.onclose = () => {
        WSconnect()
    };
}